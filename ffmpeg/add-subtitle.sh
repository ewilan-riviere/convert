#!/bin/bash
# docs: https://www.bannerbear.com/blog/how-to-add-subtitles-to-a-video-file-using-ffmpeg/

# hard subtitles
ffmpeg -i Boy.Meets.Girl.2014.mkv -vf subtitles=Boy.Meets.Girl.2014.srt Boy.Meets.Girl.2014_withsrt.mkv

# soft subtitles
ffmpeg -i Boy.Meets.Girl.2014.mkv -i Boy.Meets.Girl.2014.fr.srt -c -metadata:s:s:0 language=fre Boy.Meets.Girl.2014_withsrt.mkv

ffmpeg -i Boy.Meets.Girl.2014.mkv -i Boy.Meets.Girl.2014.en.srt -c copy -c:s mov_text Boy.Meets.Girl.2014_withsrt.mkv

ffmpeg -i Boy.Meets.Girl.2014.mkv -f srt -i Boy.Meets.Girl.2014.en.srt -map 0:0 -map 0:1 -map 1:0 -c:v copy -c:a copy -c:s srt Boy.Meets.Girl.2014_withsrt.mkv

ffmpeg -i Boy.Meets.Girl.2014.VO.mkv -i Boy.Meets.Girl.2014.fr.srt -map 0:v -map 0:a -map 1:s -c:v copy -c:a copy -c:s copy Boy.Meets.Girl.2014.VO_str.mkv
