#!/bin/bash

# get all files with `.avi` extension
for file in ffmpeg/*.avi; do
  # get the filename without the extension
  filename=$(basename "$file" .avi)
  # convert the file
  ffmpeg -fflags +genpts -i "$file" -c:v copy -c:a copy "ffmpeg/$filename.mkv"
done
