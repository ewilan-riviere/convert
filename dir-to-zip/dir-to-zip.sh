#!/bin/bash
# Transform a list of directories into a list of zip files

remove_flag="m"
recursive_flag="r"
test_flag="T"
symlink_flag="y"
compression="9"
open_flag="false"

find dir-to-zip/zip -not -name '.gitignore' -delete

for file in dir-to-zip/dirs/*; do
	echo $file
	name=$(basename "$file")

	cd dir-to-zip/dirs
	zip -"$recursive_flag$test_flag$symlink_flag$compression" "$name.zip" "$name" -x "*.DS_Store" "*[Tt]humbs.db"
	mv "$name.zip" ../zip
	cd ../..
done
