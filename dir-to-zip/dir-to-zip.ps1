# Transform a list of directories into a list of zip files

$base_path = "dir-to-zip"
Remove-Item -Path "$($base_path)\zip\*" -Exclude ".gitignore" -Recurse -Force

foreach ($file in Get-ChildItem -Path "$($base_path)\dirs\*" -Directory) {
  $full_name = $file.FullName
  Write-Output $full_name
  $name = $file.Name
  $slugify = $name -replace "[^A-Za-z0-9._-]", "-"

  Compress-Archive -Path $full_name -DestinationPath "$($base_path)\zip\$($slugify).zip" -CompressionLevel Optimal
}
