#!/bin/bash

for file in cbr-to-cbz/cbr/*.cbr; do
	echo $file

	name=$(basename "$file" .cbr)

	rm -rf "cbr-to-cbz/unrar/$name.cbz"
	rm -rf "cbr-to-cbz/unrar/$name"
	mkdir "cbr-to-cbz/unrar/$name"

	# Extract the contents of the CBR file
	unrar x "$file" "cbr-to-cbz/unrar/$name"

	# Create a CBZ archive with the same name as the CBR file
	zip -jr "cbr-to-cbz/cbz/$name.cbz" "cbr-to-cbz/unrar/$name"

	# Clean up the extracted files
	rm -rf "cbr-to-cbz/unrar/$name"
done
