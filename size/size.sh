#!/bin/bash

# sudo chmod +x ./convert.sh ; mkdir input ; mkdir output
# ./convert.sh INPUT_FORMAT OUTPUT_FORMAT HEIGHT
# ./convert.sh jpg jpg 2000

entry_format=$1
output_format=$2
height=$3
clean_input=$4

echo "Format images: $entry_format -> $output_format | Height: $height"
echo ''

echo "Clean..."
echo ''
rm -f size/output/*.*

BAR='##############################'
FILL='------------------------------'
totalLines=$(ls -1 size/input/*.$entry_format | wc -l) # num. lines in file
barLen=30

echo "Converting..."
dir="size/input"

find "$dir" -type f -print0 | while read -d $'\0' file; do

  extension="${file##*.}"

  if [ "$extension" != "$entry_format" ]; then
    continue
  fi

  count=$(($count + 1))
  percent=$((($count * 100 / $totalLines * 100) / 100))
  i=$(($percent * $barLen / 100))
  echo -ne "\r[${BAR:0:$i}${FILL:$i:barLen}] $count/$totalLines ($percent%)"

  original="$file"

  filename=$(basename "$original" ."$entry_format")

  original="$filename.$entry_format"
  newFormat="$filename.$output_format"

  oldpath="size/input/$original"
  newpath="size/output/$newFormat"

  ffmpeg \
    -hide_banner \
    -loglevel error \
    -y \
    -i "$oldpath" \
    -vf \
    "scale=-1:$height" \
    -dpi 500 \
    "$newpath"
done

if [ "$clean_input" = "true" ]; then
  echo ''
  echo ''
  echo "Clean input..."
  echo ''
  rm -f size/input/*.*
fi

echo ''
echo "Ready!"
