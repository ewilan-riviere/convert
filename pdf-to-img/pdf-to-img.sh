#!/bin/bash
# apt install poppler-utils
resolution=$1

if [ -z "$resolution" ]; then
  resolution=120
fi

volume=1
for i in pdf-to-img/pdf/*.pdf; do
  extension="${i##*.}"             # `pdf`
  filename="${i%.*}"               # `pdf-to-img/pdf/INT1 - Intégrale Tomes 1 à 3`
  basename=$(basename "$filename") # `INT1 - Intégrale Tomes 1 à 3`
  save_basename="$basename"

  save_dir="pdf-to-img/pdf/"
  saved_to="$save_dir$save_basename"

  rm -rf "$saved_to"
  mkdir "$saved_to"

  echo "Converting '$basename.pdf' to '$save_basename' ($resolution dpi)..."
  pdftoppm -jpeg -rx $resolution -ry $resolution "$filename.pdf" "$saved_to/"
  echo " "
done

# parse recursively JPG files to remove '-' beginning of the filename
echo "Cleaning filenames..."
for i in pdf-to-img/pdf/*/*.jpg; do
  path=$(dirname "$i")
  filename=$(basename "$i")
  file="$path/$filename"
  if [[ $filename == -* ]]; then
    mv "$file" "$path/${filename:1}"
  fi
done
