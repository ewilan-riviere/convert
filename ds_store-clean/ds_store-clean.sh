#!/bin/bash

# Directory to search for .DS_Store files
DIR="ds_store-clean/dirs/"

# Find and delete .DS_Store files recursively
find "$DIR" -name ".DS_Store" -type f -delete

echo ".DS_Store files deleted from $DIR"
