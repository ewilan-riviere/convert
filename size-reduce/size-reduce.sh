#!/bin/bash

input_directory="size-reduce/input"
output_directory="size-reduce/output"
max_pixels=15000

rm -f "$output_directory"/*.*

for file in "$input_directory"/*.jpg; do
  # Get current width and height
  current_dimensions=($(identify -format "%w %h" "$file"))
  width=${current_dimensions[0]}
  height=${current_dimensions[1]}

  echo "Processing $file with dimensions $width x $height"

  if [ "$height" -gt "$max_pixels" ]; then
    new_height=$max_pixels
    new_width=$(echo "scale=0; $width * $max_pixels / $height" | bc -l)
    echo "Resizing to $new_width x $new_height"
    convert "$file" -resize "$new_width"x"$new_height" "$output_directory/$(basename "$file")"
  else
    echo "No need to resize"
  fi
  echo ""
done
