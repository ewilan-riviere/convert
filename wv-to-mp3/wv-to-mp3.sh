#!/bin/bash

for i in wv-to-mp3/input/*.{wv,wav}; do
  extension="${i##*.}"
  filename="${i%.*}"
  base_name=$(basename "$filename")
  echo "Convert '$base_name.$extension' to mp3"
  rm -f "$filename.mp3"
  ffmpeg -hide_banner -loglevel error -i "$i" -codec:a libmp3lame -qscale:a 2 "$filename.mp3"
  echo "Converted!"
  mv "$filename.mp3" wv-to-mp3/output/
done
