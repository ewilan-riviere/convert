#!/bin/bash

for file in rar-to-zip/rar/*.rar; do
	echo $file

	name=$(basename "$file" .rar)
	slug=$(echo "$name" | sed 's/[^[:alnum:]]/-/g' | tr -s '-' | tr A-Z a-z)

	echo $name
	echo $slug

	rm -rf "rar-to-zip/unrar/$slug.zip"
	rm -rf "rar-to-zip/unrar/$slug"
	mkdir "rar-to-zip/unrar/$slug"

	# Extract the contents of the RAR file
	unrar x "$file" "rar-to-zip/unrar/$slug"

	# Create a CBZ archive with the same name as the CBR file
	zip -jr "rar-to-zip/zip/$slug.zip" "rar-to-zip/unrar/$slug"

	# Clean up the extracted files
	rm -rf "rar-to-zip/unrar/$slug"
done
