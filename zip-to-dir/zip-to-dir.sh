#!/bin/bash
# Transform a list of zip files into a list of directories

find zip-to-dir/dirs -not -name '.gitignore' -delete

for file in zip-to-dir/zip/*; do
	echo $file
	name=$(basename "$file")
	name_without_extension="${name%.*}"

	unzip -qq "$file" -d "zip-to-dir/dirs/$name_without_extension"
done
