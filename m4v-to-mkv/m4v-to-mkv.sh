#!/bin/bash

for file in m4v-to-mkv/m4v/*.m4v; do
  echo $file
  name=$(basename "$file" .m4v)

  ffmpeg -i "$file" -c copy "m4v-to-mkv/mkv/$name.mkv"
done
