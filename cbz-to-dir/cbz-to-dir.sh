#!/bin/bash

for file in cbz-to-dir/cbz/*.cbz; do
	echo $file

	name=$(basename "$file" .cbz)
	slug=$(echo "$name" | sed 's/[^[:alnum:]]/-/g' | tr -s '-' | tr A-Z a-z)

	mkdir -p "cbz-to-dir/cbz/$slug"
	unzip -d "cbz-to-dir/dir/$slug" "$file"
done
