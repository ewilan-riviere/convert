#!/bin/bash

for file in mov-to-mp4/mov/*.mov; do
  echo $file
  name=$(basename "$file" .mov)

  ffmpeg -i "$file" -qscale 0 "mov-to-mp4/mp4/$name.mp4"
done
