#!/bin/bash

format=$1
available_formats=("jpg" "jpeg" "png" "webp" "avif" "gif" "bmp")

if [ -z "$format" ]; then
  format="jpg"
fi

echo "Format with $format"
echo ""
rm -rf ./format/output/*

for file in ./format/input/*; do
  extension="${file##*.}"
  if [[ " ${available_formats[@]} " =~ " ${extension} " ]]; then
    base_name=$(basename "$file")
    extension="${base_name##*.}"
    filename="${base_name%.*}"

    replaced_file_path=${file/$extension/$format}
    replaced_file_path=${replaced_file_path/input/output}
    echo $replaced_file_path

    ffmpeg -loglevel quiet -i "$file" "${replaced_file_path}"

    # ffmpeg -i "$i" -vn -ar 44100 -ac 2 -b:a 192k "${name}.mp3"
    # ffmpeg -i "$i" -c copy "${name}.mp4"
    echo "Done!"
  fi
done
