#!/bin/bash
# brew install poppler for `pdftotext`

# get all files with `.pdf` extension
for file in pdf-to-text/pdf/*.pdf; do
  # get the filename without the extension
  filename=$(basename "$file" .pdf)
  # convert the file
  pdftotext "$file" "pdf-to-text/$filename.txt"
done
