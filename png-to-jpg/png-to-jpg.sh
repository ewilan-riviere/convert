#!/bin/bash

# Directory containing PNG images
input_dir="png-to-jpg/input"
output_dir="png-to-jpg/output"

# Create output directory if it doesn't exist
mkdir -p "$output_dir"

for input_file in "$input_dir"/*.png; do
  # Get the filename without extension
  filename=$(basename "$input_file" .png)

  # Output file path
  output_file="$output_dir/$filename.jpg"

  # Convert PNG to JPG with high quality
  ffmpeg -i "$input_file" -q:v 2 "$output_file"
done
