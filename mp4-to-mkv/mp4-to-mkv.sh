#!/bin/bash

for file in mp4-to-mkv/mp4/*.mp4; do
  echo $file
  name=$(basename "$file" .mp4)

  ffmpeg -i "$file" -c copy "mp4-to-mkv/mkv/$name.mkv"
done
