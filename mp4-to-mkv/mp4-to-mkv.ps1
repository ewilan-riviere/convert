$sourceFolder = "mp4-to-mkv/mp4"
$destinationFolder = "mp4-to-mkv/mkv"

# Créer le dossier de destination s'il n'existe pas
if (-not (Test-Path $destinationFolder)) {
    New-Item -ItemType Directory -Path $destinationFolder
}

# Parcourir tous les fichiers MP4 dans le dossier source
Get-ChildItem -Path $sourceFolder -Filter *.mp4 | ForEach-Object {
    $file = $_.FullName
    $name = [System.IO.Path]::GetFileNameWithoutExtension($file)
    $destination = Join-Path $destinationFolder "$name.mkv"

    Write-Host "Processing file: $file"
    ffmpeg -i $file -c copy $destination
}
