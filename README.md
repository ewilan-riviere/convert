# Convert

Some scripts to convert images or PDF.

```bash
scoop install ffmpeg # Windows with scoop

brew install ffmpeg # macOS with Homebrew

sudo apt install -y ffmpeg # Debian based with apt
```

### AVI to MKV

Convert `.avi` files to `.mkv` files

```bash
./avi-to-mkv/avi-to-mkv.sh
```

### CBR to CBZ

Convert `.cbr` files to `.cbz` files

```bash
./cbr-to-cbz/cbr-to-cbz.sh
```

### CBZ to directory

Convert `.cbz` files to directories

```bash
./cbz-to-dir/cbz-to-dir.sh
```

### Crop

Crop images

```bash
./crop/crop.sh
```

### Directories to CBZ

Convert directories to `.cbz` files

```bash
./dir-to-cbz/dir-to-cbz.sh
```

### Directories to ZIP

Convert directories to `.zip` files

```bash
./dir-to-zip/dir-to-zip.sh
```

### .DS_Store clean

Remove `.DS_Store` files

```bash
./ds_store-clean/ds_store-clean.sh
```

OR

```bash
./dir-to-zip/dir-to-zip.ps1
```

### FFMPEG

#### AVI to MKV

Convert `.avi` files to `.mkv` files

```bash
./ffmpeg/avi-to-mkv.sh
```

### Flac to MP3

Convert `.flac` files to `.mp3` files

```bash
./flac-to-mp3/flac-to-mp3.sh
```

Default bitrate is 320 kbps. To change it, pass the desired bitrate as an argument.

```bash
./flac-to-mp3/flac-to-mp3.sh 256
```

## Format

Put an image collection in `format`

```bash
./format/format.sh
```

### M4V to MP4

Convert `.m4v` files to `.mp4` files

```bash
./m4v-to-mp4/m4v-to-mp4.sh
```

### MOV to MP4

Convert `.mov` files to `.mp4` files

```bash
./mov-to-mp4/mov-to-mp4.sh
```

### MP4 to MKV

Convert `.mp4` files to `.mkv` files

```bash
./mp4-to-mkv/mp4-to-mkv.sh
```

## PDF to image

Put a PDF collection in `pdf-to-img`

Install `pdftoppm`

```bash
scoop install poppler # Windows with scoop

brew install poppler # macOS with Homebrew

sudo apt install -y poppler-utils # Debian based wit apt
```

Put `.pdf` files in `pdf-to-img/pdf` and run

```bash
./pdf-to-img/pdf-to-img.sh
```

## PDF to text

Put `.pdf` files in `pdf-to-img/pdf` and run

```bash
./pdf-to-text/pdf-to-text.sh
```

### PNG to JPG

Convert `.png` files to `.jpg` files

```bash
./png-to-jpg/png-to-jpg.sh
```

### CBZ

Convert a directory to CBZ

```bash
./dir-to-cbz/dir-to-cbz.sh dir-to-cbz/dirs/*
```

## `rar-to-zip`

Convert `.rar` files to `.zip` files

```bash
./rar-to-zip/rar-to-zip.sh
```

## Size

Put an image collection in `size`

```bash
./size/size.sh jpg jpg 800
```

## Size reduce

Put an image collection in `size-reduce`

```bash
./size-reduce/size-reduce.sh
```

## Split size

Put an image collection in `size`

```bash
./split-size/split-size.sh
```

# WV to MP3

Convert `.wv` files to `.mp3` files

```bash
./wv-to-mp3/wv-to-mp3.sh
```

## ZIP to dir

Convert `.zip` files to directories

```bash
./zip-to-dir/zip-to-dir.sh
```
