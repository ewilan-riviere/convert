#!/bin/bash
# ./crop.sh WIDTH HEIGHT X Y
# ./crop/crop.sh 1368 1920 -1 195
# Youtube Thumbnail: ./crop/crop.sh 1707 960 -1 160

width=$1
height=$2
x=$3
y=$4

echo "Crop images: $width x $height | x: $x | y: $y"

# Directory containing images
input_dir="crop/input"
output_dir="crop/output"

# Create output directory if it doesn't exist
mkdir -p "$output_dir"

default_x="(in_w-out_w)/2"
default_y="(in_h-out_h)/2"

# If width or height are not provided, keep the original dimensions
if [ -z "$width" ]; then
  width="in_w"
fi

if [ -z "$height" ]; then
  height="in_h"
fi

# If x or y equal to -1, center the crop
if [ "$x" -eq -1 ]; then
  x=$default_x
fi

if [ "$y" -eq -1 ]; then
  y=$default_y
fi

# If x or y are not provided, center the crop
if [ -z "$x" ]; then
  x=$default_x
fi

if [ -z "$y" ]; then
  y=$default_y
fi

crop_width=$width
crop_height=$height
crop_x=$x
crop_y=$y

# Crop dimensions (width:height:x:y)
crop_dimensions="$width:$height:$x:$y"
echo "Crop dimensions: $crop_dimensions"

for input_file in "$input_dir"/*.jpg; do
  # Get the filename without extension
  filename=$(basename "$input_file")

  # Output file path
  output_file="$output_dir/$filename"

  # Apply crop filter with FFmpeg
  ffmpeg -y -i "$input_file" -vf "crop=${crop_width}:${crop_height}:${crop_x}:${crop_y}" "$output_file"
done
