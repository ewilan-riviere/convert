#!/bin/bash

# sudo chmod +x ./convert.sh ; mkdir input ; mkdir output
# ./convert.sh INPUT_FORMAT OUTPUT_FORMAT HEIGHT
# ./convert.sh jpg jpg 2000

entry_format="jpg"
output_format="jpg"

echo "Clean..."
echo ''
rm -f split-size/output/*.*

BAR='##############################'
FILL='------------------------------'
totalLines=$(ls -1 split-size/input/*.$entry_format | wc -l) # num. lines in file
barLen=30

echo "Converting..."
dir="split-size/input"

function slugify {
  # Convert the string to lowercase
  slug=$(echo "$1" | tr '[:upper:]' '[:lower:]')

  # Replace non-alphanumeric characters with hyphens
  slug=$(echo "$slug" | sed -E 's/[^[:alnum:]]+/-/g')

  # Remove any leading or trailing hyphens
  slug=$(echo "$slug" | sed -E 's/^-+|-+$//g')

  echo "$slug"
}

find "$dir" -type f -print0 | while read -d $'\0' file; do

  extension="${file##*.}"

  if [ "$extension" != "$entry_format" ]; then
    continue
  fi

  count=$(($count + 1))
  percent=$((($count * 100 / $totalLines * 100) / 100))
  i=$(($percent * $barLen / 100))
  echo -ne "\r[${BAR:0:$i}${FILL:$i:barLen}] $count/$totalLines ($percent%)"

  original="$file"

  filename=$(basename "$original" ."$entry_format")
  slug=$(slugify "$filename")

  original="$filename.$entry_format"
  newFormat="$slug.$output_format"

  oldpath="split-size/input/$original"
  newpath="split-size/output/$newFormat"

  ffmpeg -hide_banner -loglevel error -y -i "$oldpath" -vf "scale=iw/2:ih/2" "$newpath"
done

if [ "$clean_input" = "true" ]; then
  echo ''
  echo ''
  echo "Clean input..."
  echo ''
  rm -f size/input/*.*
fi

echo ''
echo "Ready!"
