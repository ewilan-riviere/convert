#!/bin/bash

bitrate=$1

if [ -z "$bitrate" ]; then
  bitrate=320
fi

bitrate="$bitrate"k

input_dir="flac-to-mp3/flac"
output_dir="flac-to-mp3/mp3"

# clear output directory
rm -rf "$output_dir"
mkdir -p "$output_dir"

function iterate() {
  local dir="$1"

  for file in "$dir"/*; do
    if [ -f "$file" ]; then
      extension="${file##*.}"
      if [ ! "$extension" = "flac" ]; then
        echo "Skipping '$file'"
        continue
      fi

      relative_path="${file#$input_dir/}"
      output_file="$output_dir/${relative_path%.flac}.mp3"

      output_subdir=$(dirname "$output_file")
      if [ -d "$output_subdir" ]; then
        rm -f "$output_file"
      fi
      mkdir -p "$output_subdir"

      rm -f "$output_file"
      echo "Convert '$relative_path' to mp3 ($bitrate)"

      ffmpeg -hide_banner -loglevel error -i "$file" -ab "$bitrate" -map_metadata 0 -id3v2_version 3 "$output_file"
    else
      echo "Skipping '$file'"
    fi

    if [ -d "$file" ]; then
      iterate "$file"
      echo "Done with '$file'"
      echo ""
    fi
  done
}

iterate "$input_dir"
