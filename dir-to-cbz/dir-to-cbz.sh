#! /bin/bash

# if cbz exists, remove it
if [ -d dir-to-cbz/cbz ]; then
	rm -rf dir-to-cbz/cbz
fi
mkdir -p dir-to-cbz/cbz

dirs_path="dir-to-cbz/dirs"
echo "Creating cbz files from directories in '$dirs_path'"
echo ""

for d in $dirs_path/*/; do
	[ -L "${d%/}" ] && continue
	name=$(basename "$d")

	echo "Creating CBZ file from '$name'"

	current_dir=$(pwd)     # '/Users/ewilan/Transfer/convert'
	echo "$current_dir/$d" # '/Users/ewilan/Transfer/convert/dir-to-cbz/dirs/comics-directory'
	cd "$current_dir/$d"

	file_path="$current_dir/dir-to-cbz/cbz/${name}"

	zip -r "$file_path.cbz" *
	echo ""
	cd $current_dir
done

echo "All directories converted to CBZ successfully."
