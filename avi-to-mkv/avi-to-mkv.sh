#!/bin/bash

for file in avi-to-mkv/avi/*.avi; do
  echo $file
  name=$(basename "$file" .avi)

  ffmpeg -fflags +genpts -i "$file" -c:v copy -c:a copy "avi-to-mkv/mkv/$name.mkv"
done
