$sourceFolder = "avi-to-mkv/avi"
$destinationFolder = "avi-to-mkv/mkv"

# Ensure the destination folder exists
if (-not (Test-Path $destinationFolder)) {
    New-Item -ItemType Directory -Path $destinationFolder | Out-Null
}

# Loop through each .avi file in the source folder
foreach ($file in Get-ChildItem -Path $sourceFolder -Filter *.avi) {
    Write-Output $file.FullName
    $name = [System.IO.Path]::GetFileNameWithoutExtension($file.FullName)

    # Run ffmpeg to convert the file
    ffmpeg -fflags +genpts -i $file.FullName -c:v copy -c:a copy "$destinationFolder\$name.mkv"
}
